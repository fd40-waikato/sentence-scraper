from bs4 import BeautifulSoup
import requests

# https://www.litcharts.com/lit/the-fellowship-of-the-ring/book-1-chapter-1
# Searching for "Bilbo" on this webpage is a good test
url = input("Paste the url of the page you want to search: ")
response = requests.get(url)
# soup making time
soup = BeautifulSoup(response.content, 'html.parser')

# getting the word to search from the user
val = input("Word to search: ")  
print("Searching webpage for sentences with: " + val)

# NOTE unfortunately this scraper will search through the entire HTML for results, which means that if you look for
# 'set' it will return results such as 'var triggerOffset = title.offsetHeight + 35;' An easy way around this for now,
# but not a perfect solution, is not to search 'set' but ' set'

# create a file using the searched word as a filename
filename = val + '.csv'
f = open(filename, 'w')

# create a file using the searched word as a filename
filename = val + '.csv'
f = open(filename, 'w')

# creating the header
headers = 'Sentences containing: ' + val + '\n'
f.write(headers)

# writing the sentences to the file
for tag in soup.descendants:
    if tag.string and val in tag.string:
        f.write('...' + tag.string + '...' + '\n')
        # print(tag.string + '\n')

f.close()
